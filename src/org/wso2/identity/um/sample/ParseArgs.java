/*
 *  Copyright (c) 2015, WSO2 Inc. (http://www.wso2.org) All Rights Reserved.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package org.wso2.identity.um.sample;

import java.util.Arrays;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.kohsuke.args4j.Argument;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;
import org.kohsuke.args4j.spi.BooleanOptionHandler;

public class ParseArgs {
	@Option(name="-f",usage="File including user list")
	public File userlist;

	@Option(name="-h",usage="EMM host name", required=true)
	public String hostname = "10.251.102.20";

	@Option(name="-jksfilename",usage="JKS filename")
	public String jksfile = "wso2carbon.jks";

	@Option(name="-jkspassword",usage="JKS password")
	public String jkspassword = "wso2carbon";

	@Option(name="-adminusername",usage="Admin username")
	public String adminusername = "admin@francetv.fr";

	@Option(name="-adminpassword",usage="Admin password")
	public String adminpassword = "admin";

	@Option(name="-role",usage="Role to set")
	public String role = "Internal/store";

	// receives other command line parameters than options
	@Argument
	public List<String> arguments = new ArrayList<String>();
    
}

