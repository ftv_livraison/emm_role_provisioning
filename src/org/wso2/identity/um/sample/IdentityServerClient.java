/*
 *  Copyright (c) 2015, WSO2 Inc. (http://www.wso2.org) All Rights Reserved.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package org.wso2.identity.um.sample;

import java.util.Arrays;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.axis2.context.ConfigurationContext;
import org.apache.axis2.context.ConfigurationContextFactory;
import org.apache.axis2.transport.http.HTTPConstants;
import org.wso2.carbon.authenticator.stub.AuthenticationAdminStub;
import org.wso2.carbon.um.ws.api.WSRealmBuilder;
import org.wso2.carbon.user.core.UserRealm;
import org.wso2.carbon.user.core.UserStoreManager;

import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.CmdLineException;
import org.wso2.identity.um.sample.ParseArgs;
import org.apache.commons.io.FileUtils;

public class IdentityServerClient {
	/* How to use :
	 * 1. Please copy wso2carbon.jks from [CARBON_HOME]/repository/resources/security to
	 *    org.wso2.identity.um.sample folder.
	 * 2. change the admin username and password by changing ADMIN_USERNAME and ADMIN_PASSWORD
	 * 	  variable.
	 * 3. Change the ip address of the server url.
	 * 4. Add the users that needs to be changed to users array(Or this can be modified to read from
	 * 	  a  text file/some other resource if needed)
	 * 5. Change WSO2_CARBON_PASSWORD if  you have changed the default password.
	 */

	private static String SERVER_URL;
	// private final static String ROLE = "Internal/store";
	private static String ROLE;
	private static String ADMIN_USERNAME;
	private static String ADMIN_PASSWORD;
	private static String WSO2_CARBON_JKS;
	private static String WSO2_CARBON_JKS_PASSWORD;

	private static List<String> loadUsers ( File userFile ) throws IOException {
		return FileUtils.readLines(userFile);
	}

	private static void setVariables( ParseArgs parser ) {
		SERVER_URL = "https://" + parser.hostname + ":9443/services/";
		ROLE = parser.role;
		ADMIN_USERNAME = parser.adminusername;
		ADMIN_PASSWORD = parser.adminpassword;
		WSO2_CARBON_JKS = parser.jksfile;
		WSO2_CARBON_JKS_PASSWORD = parser.jkspassword;
	}

	public static void main(String[] args) {

		// String[] users = new String[] { "user1@wso2.com","user2@wso2.com","user3@wso2.com","user4@wso2.com"};
		// String[] users = new String[] { "si/simon.elbaz.ext@francetv.fr" };
		List<String> users;
		AuthenticationAdminStub authstub = null;
		ConfigurationContext configContext = null;
		String cookie = null;

		try {
			ParseArgs  bean = new ParseArgs();
			CmdLineParser parser = new CmdLineParser(bean);
			parser.parseArgument(args);

			System.out.println("bean.hostName:"+bean.hostname);
			setVariables(bean);
			users = loadUsers( bean.userlist );

			System.setProperty("javax.net.ssl.trustStore", WSO2_CARBON_JKS);
			System.setProperty("javax.net.ssl.trustStorePassword", WSO2_CARBON_JKS_PASSWORD);

			configContext = ConfigurationContextFactory.createConfigurationContextFromFileSystem(
					"repo", "repo/conf/client.axis2.xml");
			authstub = new AuthenticationAdminStub(configContext, SERVER_URL
					+ "AuthenticationAdmin");

			// Authenticates as a user having rights to add users.
			if (authstub.login(ADMIN_USERNAME, ADMIN_PASSWORD, null)) {
				cookie = (String) authstub._getServiceClient().getServiceContext().getProperty(
						HTTPConstants.COOKIE_STRING);

				UserRealm realm = WSRealmBuilder.createWSRealm(SERVER_URL, cookie, configContext);
				UserStoreManager storeManager = realm.getUserStoreManager();
				for(String line : users){
					System.out.println("line to process "+line);

					String[] parts = line.split(":");
					String user = "FRANCETV.FR/" + parts[0] + "@francetv.fr";
					String role = parts[1];

					if(!storeManager.isExistingUser(user)){
						System.out.println("User doesnt exist :"+user);
					}else{
						String [] roles = storeManager.getRoleListOfUser(user);
						if(roles != null && Arrays.asList(roles).contains(role)){
							System.out.println("User  :"+user+" has role "+ role +" already.");
						}else{
							System.out.println("Role  :"+role+" is assigned to user "+ user +" .");
							storeManager.updateUserListOfRole(role,null,new String[]{user} );
						}
					}
				}
				System.out.println("updated role of users");
				authstub.logout();
				System.exit(0);
			}
			System.out.println("Authentication to EMM failed. Please verify your ADMIN_USERNAME/ADMIN_PASSWORD values.");
			System.exit(1);

		} catch( CmdLineException e ) {
			System.err.println(e.getMessage());
			System.err.println("java SampleMain [options...] arguments...");
			System.err.println();
			System.exit(2);

		} catch (Exception e) {
			e.printStackTrace();
			System.exit(3);
		}

		return;
	}
}
