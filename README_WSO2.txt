How to use :
	1. Please add axis2_1.6.1.wso2v10(attatched), in to libs folder of the attatched project
	2. Please copy wso2carbon.jks from [CARBON_HOME]/repository/resources/security to
		org.wso2.identity.um.sample folder.
	3. change the admin username and password by changing ADMIN_USERNAME and ADMIN_PASSWORD
		variable.
	4. Change the ip address of the server url.
	5. Add the users that needs to be changed to users array(Or this can be modified to read from
		a  text file/some other resource if needed)
	6. Change WSO2_CARBON_PASSWORD if  you have changed the default password.
