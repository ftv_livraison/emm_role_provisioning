#!/bin/bash

export WORK_DIR=`dirname $0`
export LIB_PATH=`echo ${WORK_DIR}"/libs"`
export EMM_HOST="10.251.102.22"
export ADMIN_USERNAME="admin@francetv.fr"
export ADMIN_PASSWORD="f.I+>qLS"
export JKS_FILE="wso2carbon_int.jks"
export JKS_PASSWORD="wso2carbon"

export RUNDATE=`date +%Y%m%d%H%M%S`

java \
-cp ${LIB_PATH}/XmlSchema_1.4.7.wso2v2.jar:${LIB_PATH}/axiom_1.2.11.wso2v4.jar:${LIB_PATH}/axis2_1.6.1.wso2v10.jar:${LIB_PATH}/commons-codec_1.4.0.wso2v1.jar:${LIB_PATH}/commons-fileupload_1.2.2.wso2v1.jar:${LIB_PATH}/commons-httpclient_3.1.0.wso2v2.jar:${LIB_PATH}/httpcore_4.1.0.wso2v1.jar:${LIB_PATH}/neethi_2.0.4.wso2v4.jar:${LIB_PATH}/org.wso2.carbon.authenticator.proxy_4.2.0.jar:${LIB_PATH}/org.wso2.carbon.authenticator.stub_4.2.0.jar:${LIB_PATH}/org.wso2.carbon.logging_4.2.0.jar:${LIB_PATH}/org.wso2.carbon.um.ws.api_4.2.1.jar:${LIB_PATH}/org.wso2.carbon.user.api_4.2.0.jar:${LIB_PATH}/org.wso2.carbon.user.core_4.2.0.jar:${LIB_PATH}/org.wso2.securevault_1.0.0.wso2v2.jar:${LIB_PATH}/wsdl4j_1.6.2.wso2v4.jar:${WORK_DIR}/src/org/wso2/identity/um/sample/IdentityServerClient.class:${LIB_PATH}/args4j-2.32.jar:${LIB_PATH}/commons-io-2.4.jar:. \
org.wso2.identity.um.sample.IdentityServerClient -h ${EMM_HOST} -adminusername ${ADMIN_USERNAME} -adminpassword ${ADMIN_PASSWORD} -jksfilename ${JKS_FILE} -jkspassword ${JKS_PASSWORD} -f ./user_list_example.txt > ${WORK_DIR}"/logs/role_provisioning_"${RUNDATE}".log" 2>&1
