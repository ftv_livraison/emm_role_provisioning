Mode d emploi
-------------

1- Renseigner les variables dans run.sh:
	* EMM_HOST: serveur EMM
	* ADMIN_USERNAME
	* ADMIN_PASSWORD
	* JKS_FILE
	* JKS_PASSWORD
2- Renseigner la liste des utilisateurs user_list_example.txt au format
<utilisateur>:<role assigne>
3- Remplacer dans ./repo/conf/client.axis2.xml les variables avec les valeurs
correspondantes a l'environnement:
    <parameter name="userName">admin@francetv.fr</parameter>
    <parameter name="password">admin</parameter>
4- Lancer le script run.sh correspondant a l environnement
